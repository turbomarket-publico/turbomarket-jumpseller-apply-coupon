if(!window.Turbomarket) {
  window.Turbomarket = {};
}

// IMPORTANT: you must create a "false product" with a price of $0 and set
// window.Turbomarket.emptyProductId with the id of that product
if(!window.Turbomarket.emptyProductId) {
  window.Turbomarket.emptyProductId = 1;
}

var emptyProductId = window.Turbomarket.emptyProductId;

// function that applies the discount using a coupon
// (as an argument you must provide the coupon code)
window.Turbomarket.applyDiscount =  function (discount) {
  Jumpseller.getCart({ callback: function (cart) {
    if(cart.id) {
      Jumpseller.addCouponToCart(discount);
    } else {
      Jumpseller.addProductToCart(emptyProductId, 1, {}, { callback: function () {
        Jumpseller.getCart({ callback: function (cart) {
          cart.products.forEach(function (product) {
            if(product.product_id === emptyProductId) {
              Jumpseller.updateCart(product.id, 0, { callback: function () {
                Jumpseller.addCouponToCart(discount);
              }});
            }
          });
        }});
      }});
    }
  }});
};

// let's check if there's a discount in the URL (...?discount=COUPON)
try {
  var discount = new URLSearchParams(document.location.search).get('discount');
  if(discount) {
    var interval = window.setInterval(function () {
      if(Jumpseller) {
        window.Turbomarket.applyDiscount(discount);
        window.clearInterval(interval);
      }
    }, 500);
  }
} catch(e) {}