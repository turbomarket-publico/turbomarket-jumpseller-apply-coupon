Código Javascript para poder aplicar cupones de descuento en Jumpseller, aunque el cliente no haya inicializado un carro (se inicializará uno por él)

Además, permite que se auto-aplique un cupón que venga indicado en la URL, con el parámetro "discount".

Por ejemplo: http://tienda.com/?discount=CUPON